/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import '../vendor/fortawesome/font-awesome/js/all.min.js'

// start the Stimulus application
import './bootstrap';
require('bootstrap');


function media992() {

    let drop = Array.from(document.getElementsByClassName('menu-dropdown'));

    if (window.matchMedia("(max-width: 992px)").matches) { // If media query matches
        drop.forEach( function(e) {
            e.classList.replace("dropend", "dropdown");
        });
    }
    else {
        drop.forEach( function(e) {
            e.classList.replace("dropdown", "dropend");
        });
    }
}


window.addEventListener('resize', media992);
media992();