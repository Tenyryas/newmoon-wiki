<?php

namespace App\Controller;

use DateTime;
use DateTimeZone;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleCategoryRepository;
use App\Repository\ArticleRepository;
use App\Repository\GameSettingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @Route("/articles")
 */
class ArticleController extends AbstractController
{

    private $slugger;

    /**
     * @Route("/mes_articles", name="articles_user", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('article/list.html.twig', [
            'articles' => $articleRepository->findByUser( $this->getUser() ),
            'setting' => "Mes articles"
        ]);
    }

    /**
     * @Route("/liste", name="articles_list", methods={"GET"})
     */
    public function list(Request $request, ArticleRepository $articleRepository, ArticleCategoryRepository $articleCatRepo, GameSettingRepository $settingRepo): Response
    {   

        $querySetting = $request->query->get('setting');
        $setting = $settingRepo->findOneBy(array ( "name" => $querySetting ));

        // si $setting est null, renvoyer un 404
        if( !$setting ) {
            throw $this->createNotFoundException("L'univers n'existe pas");
        }

        $queryCat = $request->query->get('cat');

        // si queryCat renvoie quelque chose, procéder normalement
        if ( $queryCat != null) {

            $cat = $articleCatRepo->findOneBy(array ( 'type' => $queryCat ));

            // si $cat est null, renvoyer un 404
            if( !$cat ) {
                throw $this->createNotFoundException("La catégorie n'existe pas");
            }

            $articles = $articleRepository->findBySettingAndCat($setting, $cat);
    
            return $this->render('article/list.html.twig', [
                'articles' => $articles,
                'setting' => $request->query->get('setting'),
                'cat' => $request->query->get('cat')
            ]);    
        }
        // sinon, ne renvoyer que setting
        else {
            $articles = $articleRepository->findBy(array ( 'gameSetting' => $setting ));
    
            return $this->render('article/list.html.twig', [
                'articles' => $articles,
                'setting' => $request->query->get('setting'),
            ]);    
        }
    }

    public function randFive (ArticleRepository $articleRepository)
    {

        $articles = $articleRepository->findRand(5);
    
        return $this->render('article/list_rand.html.twig', [
            'articles' => $articles,
            'setting' => '5 articles au hasard',
        ]);  

    }


    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $article = new Article($this->getUser());
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $this->slugger = new AsciiSlugger();

            $article->setDateCreated( new DateTime('now', new DateTimeZone('Europe/Paris')) )
                    ->setSlug( $this->slugger->slug($article->getTitle()) );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('articles_user', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/new.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }



    /**
     * @Route("-{setting}/{category}/{id}-{title}", name="article_show", methods={"GET"})
     */
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }



    /**
     * @Route("-{setting}/{category}/{id}/edit", name="article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->slugger = new AsciiSlugger();

            $article->setDateModified( new DateTime('now', new DateTimeZone('Europe/Paris')) )
                    ->setSlug( $this->slugger->slug($article->getTitle()) );

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_show', [
                'id' => $article->getId(),
                'setting' => $article->getGameSetting()->getName(),
                'category' => $article->getArticleCategory()->getType(),
                'title' => $article->getTitle(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }



    /**
     * @Route("-{setting}/{category}/{id}", name="article_delete", methods={"POST"})
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('articles_user', [], Response::HTTP_SEE_OTHER);
    }
}
