<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="home")
     *
     * @return Response
     */
    public function bienvenue()
    {
        return $this->render("base.html.twig");
    }


        /**
     * @Route("/admin/dash", methods={"GET"}, name="admin_dash")
     *
     * @return Response
     */
    public function adminDash()
    {
        return $this->render("admin.html.twig");
    }

}

