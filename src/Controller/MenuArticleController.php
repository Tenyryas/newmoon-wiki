<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\GameSetting;
use App\Entity\ArticleCategory;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MenuArticleController extends AbstractController
{

    public function menuRender()
    {
        
        $settings = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(GameSetting::class)
                        ->findAll();

        $categories = $this->getDoctrine()
                    ->getManager()
                    ->getRepository(ArticleCategory::class)
                    ->findAll();

        $articles = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(Article::class);

        return $this->render('partials/__menuArticles.html.twig', [
            'settings' => $settings,
            'categories' => $categories,
            'articles' => $articles
        ]);
    }
}
