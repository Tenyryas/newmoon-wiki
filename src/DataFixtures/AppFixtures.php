<?php

namespace App\DataFixtures;

use App\Entity\ArticleCategory;
use App\Entity\GameSetting;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setDisplayname("Admin")
            ->setEmail("admin@admin.fr")
            ->setPassword($this->passwordHasher->hashPassword($user, '0000'))
            ->setRoles(array('ROLE_USER'));

        $manager->persist($user);

        $user2 = new User();
        $user2->setDisplayname("Kyane")
            ->setEmail("admin@free.fr")
            ->setPassword($this->passwordHasher->hashPassword($user2, '0000'))
            ->setRoles(array('ROLE_USER'));

        $manager->persist($user2);

        $gameSetting = new GameSetting();
        $gameSetting->setName('Hellfrost');
        $manager->persist($gameSetting);

        $articleType = new ArticleCategory();
        $articleType->setType('Personnage');
        $manager->persist($articleType);

        $manager->flush();
    }
}
