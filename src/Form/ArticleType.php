<?php

namespace App\Form;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use App\Entity\Article;
use App\Entity\GameSetting;
use App\Entity\ArticleCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('imgSrc', ElFinderType::class, [
                'instance' => 'form', 
                'enable' => true,
                'required' => false
            ])
            ->add('gameSetting', EntityType::class, [ 
                'class' => GameSetting::class,
                'choice_label' => 'name',
            ])
            ->add('articleCategory', EntityType::class, [ 
                'class' => ArticleCategory::class,
                'choice_label' => 'type',
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['class' => 'tinymce',
                            'data-theme' => 'advanced'
                        ],
            ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
