<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\GameSetting;
use App\Entity\ArticleCategory;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * Returns an array of Article objects
    //  * where the author is $user
    //  * @return Article[]
    //  * 
    //  */ 
    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.author = :author')
            ->setParameter('author', $user)
            ->orderBy('a.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    
    // /**
    //  * Returns an array of Article objects
    //  * of GameSetting $setting and ArticleCategory $cat
    //  * @return Article[]
    //  * 
    //  */ 
    public function findBySettingAndCat(GameSetting $setting, ArticleCategory $cat)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.gameSetting = :setting', 'a.articleCategory = :cat')
            ->setParameter('setting', $setting)
            ->setParameter('cat', $cat)
            ->orderBy('a.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    // /**
    //  * Returns an array of 5 random Article objects
    //  * @return Article[]
    //  * 
    //  */ 
    public function findRand(int $limit)
    {
        $conn = $this->getEntityManager()
                        ->getConnection();

        $maxStmt = $conn->prepare(
            'SELECT id FROM article ORDER BY RAND() LIMIT 5'
            );

        $maxStmt->executeQuery();
        $rand = $maxStmt->fetchAllNumeric();

        return $this->createQueryBuilder('a')
            ->where('a.id = :rand
                    OR a.id = :rand2 
                    OR a.id = :rand3 
                    OR a.id = :rand4 
                    OR a.id = :rand5')
            ->setParameter('rand', $rand[0][0])
            ->setParameter('rand2', $rand[1][0])
            ->setParameter('rand3', $rand[2][0])
            ->setParameter('rand4', $rand[3][0])
            ->setParameter('rand5', $rand[4][0])
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
