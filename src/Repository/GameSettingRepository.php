<?php

namespace App\Repository;

use App\Entity\GameSetting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameSetting|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameSetting|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameSetting[]    findAll()
 * @method GameSetting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameSettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameSetting::class);
    }

    // /**
    //  * @return GameSetting[] Returns an array of GameSetting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameSetting
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
